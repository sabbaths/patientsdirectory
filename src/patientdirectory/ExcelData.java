package patientdirectory;


import java.util.ArrayList;

class ExcelData {
    private Object[][] data;
    private ArrayList<String> columns;
    private int rowCounter;
    
    public Object[][] getData() {
        return data;
    }
    
    public int getLastID() {
        int lastID = Integer.parseInt(data[data.length-1][0].toString());
        lastID = data.length;
        return lastID;
    }

    public String[] getColumns () {
        try {
            //System.out.println(columns.size());
            String[] returnArray = new String[columns.size()];
            for(int i=0;i<columns.size();i++) {
                returnArray[i] = columns.get(i).toUpperCase();
            }
            return returnArray;            
        } catch(Exception e) {
            System.out.println(e);
        }
        String[] test = new String[1];
        return test;
    }    
    
    void setColumnNames(ArrayList<String> columnNames) {
        columns = columnNames;
    }
    
    void setRows(int i) {
        rowCounter = 0;
        data = new Object[i][columns.size()];
    }
    
    void pushRow(String[] row) {
        try {
            data[rowCounter] = row;
            rowCounter++;
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }
    
    void getObjectCount() {
        //System.out.println("data length: " +data.length);
    }
    
    public int getCount() {
        try {
            return columns.size();
        } catch(Exception e) {
            System.out.println("Exception get count");
            return 0;
        }
        
    }
}
