/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientdirectory;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author sabbaths
 */
public class Database {
    private Fillo fillo;
    private Connection connection;
    private String dbPath;
    
    public Database() {
        try {
            //locate db
            //create connection
            dbPath = System.getProperty("user.dir")+"/src/patientdirectory/db.xlsx";
            fillo=new Fillo();
            connection=fillo.getConnection(dbPath);
            System.out.println("Database Connection Successful");
        } catch (Exception e) {
            try {
                //JOptionPane.showMessageDialog(null, e);
                dbPath = System.getProperty("user.dir")+"/db.xlsx";
                fillo=new Fillo();
                connection=fillo.getConnection(dbPath);
                System.out.println("Database Connection Successful2");
            } catch (Exception e2) {
                JOptionPane.showMessageDialog(null, e + " "+ e2);
            }
        }
        //for test
        //getHistory(1,1);
    }
    
    public void closeDBConnection() {
        connection.close();
    }
    
    //Query List of Patients 
    public ExcelData getPatients(String queryLastName) {
        ExcelData ed = new ExcelData();
        try {
            String strQuery = queryLastName == "" 
                    ? "Select * from Patients ORDER BY id" 
                    : "Select * from Patients where last like '" +queryLastName+ "%'";
            
            Recordset recordset=connection.executeQuery(strQuery);
            
            ed.setColumnNames(recordset.getFieldNames());
            ed.setRows(recordset.getCount());

            while(recordset.next()){
                String[] rowArray = new String[ed.getCount()];

                int i = 0;
                for (String strColumn:recordset.getFieldNames()) {
                    rowArray[i] = recordset.getField(strColumn);
                    i++;
                }
                ed.pushRow(rowArray);
            }
            recordset.close();            
        } catch (Exception e) {
            //System.out.println(e);
            ArrayList<String> columns = new ArrayList();
            columns.add("NO DATA");
            columns.add("NO DATA");
            ed.setColumnNames(columns);
            ed.setRows(1);
            
            String[] rowArray = new String[2];
            rowArray[0] = "0";
            rowArray[1] = "NO DATA";
            ed.pushRow(rowArray);
            
            System.out.println(e);
        }
        //System.out.println("Count::"+ed.getCount());
        return ed;
    }
    
    //Query Patient Table History
    public ExcelData getHistory(int tab_id, int patient_id) {
        String strQuery= "";
        
        switch(tab_id) {
            case 3: strQuery="Select name,date,doctor,col1,col2,col3 from Medical_History where patient_id=" + patient_id; break;
            case 4: strQuery="Select name,date,doctor,col1,col2,col3 from allergies where patient_id=" + patient_id; break;
            case 5: strQuery="Select name,date,doctor,col1,col2,col3 from surgeries where patient_id=" + patient_id; break;
            case 6: strQuery="Select name,date,doctor,col1,col2,col3 from family_history where patient_id=" + patient_id; break;
            case 7: strQuery="Select name,date,doctor,col1,col2,col3 from social_history where patient_id=" + patient_id; break;
            case 8: strQuery="Select name,date,doctor,col1,col2,col3 from illness where patient_id=" + patient_id; break;
            case 9: strQuery="Select name,date,doctor,col1,col2,col3 from medicines where patient_id=" + patient_id; break;
            case 10: strQuery="Select name,date,doctor,col1,col2,col3 from bp where patient_id=" + patient_id; break;
        }
        //System.out.println("QUERY:" + strQuery);
        ExcelData ed = new ExcelData();
        try {
            //strQuery="Select * from Medical_History where patient_id=" + 1;
            Recordset recordset=connection.executeQuery(strQuery);
            
            ed.setColumnNames(recordset.getFieldNames());
            ed.setRows(recordset.getCount() + 10);

            while(recordset.next()){
                String[] rowArray = new String[ed.getCount()];

                int i = 0;
                for (String strColumn:recordset.getFieldNames()) {
                    rowArray[i] = recordset.getField(strColumn);
                    i++;
                }
                ed.pushRow(rowArray);
            }
            
            recordset.close();         
        } catch (Exception e) {
            //System.out.println(e);
            ArrayList<String> columns = new ArrayList();
            columns.add("Name");
            columns.add("Date");
            columns.add("Doctor");
            columns.add("COL1");
            columns.add("COL2");
            columns.add("COL3");
            ed.setColumnNames(columns);
            ed.setRows(60);
            
            for(int i=1;i<20;i++) {
                String[] rowArray = new String[2];
                rowArray[0] = "";
                rowArray[1] = "";
                ed.pushRow(rowArray);
            }
        }
        //System.out.println("Count::"+ed.getCount());
        return ed;
    }    

    public ArrayList<String> getPatientsData(int patientID) {
        System.out.println("Get Patients Data: ID: " + patientID);
        ArrayList<String> patientsArray = new ArrayList();
        
        try {
            String strQuery="Select * from Patients where id = " + patientID;
            Recordset recordset=connection.executeQuery(strQuery);

            while(recordset.next()){
                for (String strColumn:recordset.getFieldNames()) {
                    patientsArray.add(recordset.getField(strColumn));   
                }
            }
            recordset.close();   
            System.out.println("Get Patients Data Success: " + patientID);
        } catch (Exception e) {
            System.out.println("Get Patients Data: ID: " + e);
        }
        
        return patientsArray;
    }
    //save personal info to db
    public int savePersonalInfo(int patientID, String tfFirst, String tfMiddle, 
            String taAddress, String tfLast, 
            String tfBT, String tfSN, 
            String tfAge, String tfBDay, String tfGender,
            String unit, String unitAddress, String rank, String religion) {
        int newPatientID = 0;
        
        try {
            if(patientID == 0) {
                ExcelData ed = this.getPatients("");
                newPatientID = patientID = ed.getLastID()+1;
                System.out.println("NEW ID" + patientID);
            } else {
                newPatientID = patientID;
            }


            String strQuery="INSERT INTO Patients(First,"
                    + "Middle,"
                    + "Last,"
                    + "age, birthday, "
                    + "serial_number,"
                    + "blood_type, "
                    + "gender, address, "
                    + "unit, unit_address, "
                    + "rank, religion, "
                    + "id) "
                    + "VALUES( '" + tfFirst +"'"
                    + "       ,'" + tfMiddle +"'"
                    + "       ,'" + tfLast +"'"
                    + "       ,'" + tfAge +"'"
                    + "       ,'" + tfBDay +"'"
                    + "       ,'" + tfSN +"'"
                    + "       ,'" + tfBT +"'"
                    + "       ,'" + tfGender +"'"
                    + "       ,'" + taAddress +"'"
                    + "       ,'" + unit +"'"
                    + "       ,'" + unitAddress +"'"
                    + "       ,'" + rank +"'"
                    + "       ,'" + religion +"'"
                    + "       ,'" + patientID +"'"
                    + ")"
                    + "";
            System.out.println(strQuery);
            connection.executeUpdate(strQuery);
        } catch(Exception e) {
            System.out.println(e);
        }
        
        return newPatientID;
    }
    //save table history
    void saveMedicalHistory(int patientID, JTable table, String sheet) {
        System.out.println("<---saveMedicalHistory " + patientID + " " + sheet + " " + sheet + " ROW COUNT: " +table.getRowCount());
        try {
            //dbStatement=con.createStatement();
            //JOptionPane.showMessageDialog(null, "SAVE PATIENT " + table.getRowCount());
            for(int i=0;i<table.getRowCount();i++){

                String id="1";
                String name="";
                String date="NA";
                String doc="NA";
                String col1="NA";
                String col2="NA";
                String col3="NA";
                int pID = patientID;
                
                try {name=table.getValueAt(i, 0).toString();} catch(Exception e) {}
                try {date=table.getValueAt(i, 1).toString();} catch(Exception e) {}
                try {doc=table.getValueAt(i, 2).toString();} catch(Exception e) {}
                try {col1=table.getValueAt(i, 3).toString();} catch(Exception e) {}
                try {col2=table.getValueAt(i, 4).toString();} catch(Exception e) {}
                try {col3=table.getValueAt(i, 5).toString();} catch(Exception e) {}
                
                if(name.trim().length() == 0) {
                    continue;
                }
                
                String strQuery="INSERT INTO " +sheet+" (id,patient_id,name,date,doctor, col1,col2,col3) "
                        + "VALUES('" + id +"'"
                        + ",'" + pID +"'"
                        + ",'" + name +"'"
                        + ",'" + date +"'"
                        + ",'" + doc +"'"
                        + ",'" + col1 +"'"
                        + ",'" + col2 +"'"
                        + ",'" + col3 +"'"
                        + ")"
                        + "";
                
                connection.executeUpdate(strQuery);  
                System.out.println(strQuery);
            }
        } catch (Exception e) {
            System.out.println("saveMedicalHistory exception " + e);
        }
    }
    //delete all table history of patient
    void deletePatientFiles(int patientID) {
        
        try {
            executeDeleteQuery("DELETE FROM Patients WHERE id =" + patientID);
            executeDeleteQuery("DELETE FROM Patients WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Medical_History WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Allergies WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Surgeries WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Family_History WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Social_History WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Illness WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM Medicines WHERE patient_id =" + patientID);
            executeDeleteQuery("DELETE FROM BP WHERE patient_id =" + patientID);
            //connection.executeUpdate(strQuery);        
            //System.out.println("DELETE SUCCES: " + strQuery);            
        } catch (Exception e) {
            System.out.println("DELETE PATIENT ROW: "+e);
        }
    }
    
    void executeDeleteQuery(String query) {
        try {
            connection.executeUpdate(query);        
            System.out.println("DELETE SUCCES: " + query);   
        } catch (Exception e) {
            System.out.println("DELETE PATIENT ROW: "+e);
        }
    }
}