/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientdirectory;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 *
 * @author sabbaths
 */
public class Login extends Frame implements ActionListener { 
    private Label lblTitle;
    private Label lblUsername;
    private Label lblPassword;
    private TextField tfUser;
    private TextField tfPass;
    private Button loginButton;
    
    public Login() {
        new PatientFile();
        new Database();
        
        JPanel contentPanetest = new JPanel(new FlowLayout());
        JPanel contentPane = new JPanel(new FlowLayout());
        this.setLayout(new GridLayout(0,1));
        //lblTitle = new Label("Patient Directory");
        //lblTitle.setSize(250,250);
        lblUsername = new Label("Username: ");  
        tfUser = new TextField(25);
        lblPassword = new Label("Password: ");  
        tfPass = new TextField(25);
        loginButton = new Button("Login");
        
        loginButton.addActionListener(this);

        contentPane.add(lblUsername); 
        contentPane.add(tfUser);
        contentPane.add(lblPassword);         
        contentPane.add(tfPass);
        contentPane.add(loginButton);
        contentPanetest.setSize(200, 250); 
        contentPanetest.setBackground(Color.black);
        contentPane.setSize(150, 150); 
        this.add(contentPanetest);
        this.add(contentPane);

        this.setTitle("Login");  // "super" Frame sets its title
        this.setSize(600, 500); 
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        //this.setAlwaysOnTop(true);
    }
    
   @Override
   public void actionPerformed(ActionEvent evt) {     
       System.out.println("Login");
   }
}
