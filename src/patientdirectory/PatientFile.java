/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientdirectory;

import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javafx.scene.control.DatePicker;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.jdatepicker.DateModel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;


/**
 *
 * @author sabbaths
 */
public class PatientFile extends JPanel {
    private Database database;
    private JTabbedPane tabbedPane;
    JTable tablePatients;
    
    private JTable JThistory;
    private JTable JTfamily;
    private JTable JTsocial;
    private JTable JTallergies;
    private JTable JTsurgeries;
    private JTable JTmedicines;
    private JTable JTillness;
    private JTable JTBP;
    private int patientID;
    
    private TextField tfFirst;
    private JLabel labelMiddle;
    private TextField tfMiddle;
    private JLabel labelLast;
    private JLabel lblAddress;
    private TextField taAddress;
    private TextField tfLast;
    private ButtonGroup group;
    private JRadioButton rdBtnM;
    private JRadioButton rdBtnFM;
    private JLabel lblBT;
    private JLabel lblSN;
    private TextField tfBT;
    private TextField tfSN;
    private JLabel lblAge;
    private TextField tfAge;
    private JLabel lblBDay;
    private TextField tfBDay;
    private JLabel lblUnit;
    private TextField tfUnit;
    private JLabel lblUnitAdd;
    private TextField tfUnitAdd;
    private JLabel lblRank;
    private TextField tfRank;
    private JLabel lblReligion;
    private TextField tfReligion;
    
    public PatientFile() {
        super(new GridLayout(1, 1));
        JFrame frame = new JFrame("Medical");
        this.patientID = 0;
        database = new Database();

        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Patients", createListofPatientsTab(""));
        tabbedPane.addTab("Personal Information", createPersonalInfoTab(""));   
        tabbedPane.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                    System.out.println("CHANGE TAB");
                    if(tabbedPane.getSelectedIndex() == 0) {
                        /*
                        System.out.println("CHANGE TAB RELOAD TAB 0");
                        ExcelData ed = database.getPatients();
                        String[] columnNames = ed.getColumns();
                        Object[][] data = ed.getData();

                        TableModel newModel = new DefaultTableModel(data, columnNames);

                        tablePatients.setModel(newModel);
                        ((AbstractTableModel) newModel).fireTableDataChanged();
                        */
                        reloadListofPatientsTable(tablePatients, "");
                        patientID = 0;
                        retrievePatientFile(patientID, 1);  
                    } else {
                        //patientID = 0;                    
                        retrievePatientFile(patientID, 1);                    
                    }
            }
            
            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

        });
        
        frame.add(tabbedPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        frame.setVisible(true);
    }

    void reloadListofPatientsTable(JTable tableToReload, String queryName) {
        try {
            System.out.println("RELOAD PATIENTS TABLE: SEARCH " +queryName);
            ExcelData ed = database.getPatients(queryName);
            String[] columnNames = ed.getColumns();
            Object[][] data = ed.getData();

            TableModel newModel = new DefaultTableModel(data, columnNames);

            tableToReload.setModel(newModel);
            ((AbstractTableModel) newModel).fireTableDataChanged(); 
            System.out.println("END RELOAD PATIENTS TABLE: SEARCH " +queryName);            
        } catch(Exception e) {
            System.out.println("reloadListofPatientsTable" + e);
        }
    }
    
    private JComponent createListofPatientsTab(String title) {
        JComponent panel1 = makeTextPanel(title);
        
        ExcelData ed = database.getPatients("");
        String[] columnNames = ed.getColumns();
        Object[][] data = ed.getData();

        JLabel jl = new JLabel("Search Last Name");
        JTextField jtf = new JTextField(30);
        JButton jbSearch = new JButton("Search");
        JComponent panel2 = makeTextPanel(title);
        panel2.setLayout(new BorderLayout());
        panel2.add(jl, BorderLayout.WEST);
        panel2.add(jtf, BorderLayout.CENTER);
        panel2.add(jbSearch, BorderLayout.LINE_END);
        
        jbSearch.addActionListener(new ActionListener() { 
          public void actionPerformed(ActionEvent e) {
            String searchLastName = jtf.getText();
            
            if(jtf.getText().trim().length() != 0) {
              reloadListofPatientsTable(tablePatients, searchLastName);
            } else {
              reloadListofPatientsTable(tablePatients, "");
            }
          } 
        } );
        
        tablePatients = new JTable(data, columnNames) {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        
        tablePatients.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int row = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2) {
                    
                    int rowNumber = table.getSelectedRow();
                    //int columnNumber = table.getSelectedColumn();
                    patientID = Integer.parseInt(table.getValueAt(rowNumber, 0).toString());
                    //System.out.println("VALUE: " + table.getValueAt(rowNumber, 0));
                    retrievePatientFile(patientID, 1);
                }
            }
        });
        
        tablePatients.setGridColor(Color.BLACK);
        tablePatients.setBorder(new EtchedBorder(EtchedBorder.RAISED));
        
        JScrollPane scrollPane = new JScrollPane(tablePatients);
        scrollPane.setVisible(true);
        tablePatients.setFillsViewportHeight(true);
        panel1.setLayout(new BorderLayout());
        panel1.add(panel2, BorderLayout.PAGE_END);
        panel1.add(tablePatients.getTableHeader(), BorderLayout.PAGE_START);
        //panel1.add(tablePatients, BorderLayout.CENTER);
        
        
        panel1.add(scrollPane, BorderLayout.CENTER);
        
        return panel1;
    }
    
    void setTableCellBorder(JTable table) {
        table.setGridColor(Color.BLACK);
        table.setBorder(new EtchedBorder(EtchedBorder.RAISED));
    }
    
    private JComponent createPersonalInfoTab(String title) {
        JTabbedPane tabbedPane1 = new JTabbedPane();
        
        PanelandTable patHistory = createHistoryTab("", 3);
        JComponent JPHistory = patHistory.getPanel();
        JThistory = patHistory.getTable();
        tabbedPane1.addTab("Medical History", JPHistory);
        
        PanelandTable patAllergies = createHistoryTab("", 4);
        JComponent JPAllergies = patAllergies.getPanel();
        JTallergies = patAllergies.getTable();
        tabbedPane1.addTab("Allergies", JPAllergies);
        
        PanelandTable patSurgeries = createHistoryTab("", 5);
        JComponent JPSurgeries = patSurgeries.getPanel();
        JTsurgeries = patSurgeries.getTable();
        tabbedPane1.addTab("Surgeries", JPSurgeries);
        
        PanelandTable patFHistory = createHistoryTab("", 6);
        JComponent JPFHistory = patFHistory.getPanel();
        JTfamily = patFHistory.getTable();
        tabbedPane1.addTab("Family History", JPFHistory);
        
        PanelandTable patPSH = createHistoryTab("", 7);
        JComponent JPPSH = patPSH.getPanel();
        JTsocial = patPSH.getTable();
        tabbedPane1.addTab("Personal Social History", JPPSH);
        
        PanelandTable patIllness = createHistoryTab("", 8);
        JComponent JPIllness = patIllness.getPanel();
        JTillness = patIllness.getTable();
        tabbedPane1.addTab("Present Illness", JPIllness);
        
        PanelandTable patMeds = createHistoryTab("", 9);
        JComponent JPMeds = patMeds.getPanel();
        JTmedicines = patMeds.getTable();
        tabbedPane1.addTab("List of Medicines", JPMeds);
        
        PanelandTable patBP = createHistoryTab("", 10);
        JComponent JPBP = patBP.getPanel();
        JTBP = patBP.getTable();
        tabbedPane1.addTab("Blood Pressure and Date", JPBP);
        
        
        JComponent panel = new JPanel(new BorderLayout());
        JComponent panelInner = new JPanel(new GridLayout(0,2));
        JComponent panelOuter = new JPanel(new GridLayout(1, 1));
        JComponent panelCenter = new JPanel(new GridLayout(1, 1));
        panelInner.setBorder(BorderFactory.createEmptyBorder(0,10,10,10));
        /*
        tabbedPane.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                System.out.println("Tab: " + tabbedPane.getSelectedIndex());
                JOptionPane.showMessageDialog(null, "Tab: " + tabbedPane.getSelectedIndex());
            }
        }); */ 
        
        JButton btnSave = new JButton("SAVE");
        btnSave.setBackground(Color.GRAY);
        
        btnSave.addActionListener(new ActionListener() { 
          public void actionPerformed(ActionEvent e) { 
            if(!nullFields()) {
                
                if(patientID != 0) {
                    deletePatient(patientID);
                }
                savePatient(patientID);
                
                JOptionPane.showMessageDialog(null, "SAVED");
            }
          } 
        } );
        
        panelOuter.add(btnSave); //panelOuter.add(test1);

        JButton jb = new JButton("btn test");
        JLabel labelFirst = new JLabel("*First Name: ");
        tfFirst = new TextField(20);
        JLabel labelMiddle = new JLabel("*Middle Name: ");
        tfMiddle = new TextField(20);
        JLabel labelLast = new JLabel("*Last Name: ");
        JLabel lblAddress = new JLabel("*Home Address: ");
        taAddress = new TextField(20);
        tfLast = new TextField(20);
        ButtonGroup group = new ButtonGroup();
        rdBtnM = new JRadioButton("*Male:");
        rdBtnFM = new JRadioButton("*Female:");
        JLabel lblBT = new JLabel("*Blood Type: ");
        JLabel lblSN = new JLabel("*Serial Number: ");
        tfBT = new TextField(20);
        tfSN = new TextField(20);
        JLabel lblAge = new JLabel("*Age: ");
        tfAge = new TextField(2);
        JLabel lblBDay = new JLabel("*Birthday: ");
        tfBDay = new TextField(10);
        lblUnit = new JLabel("*Unit: ");
        tfUnit = new TextField(10);
        lblUnitAdd = new JLabel("*Unit Address: ");
        tfUnitAdd = new TextField(10);
        lblRank = new JLabel("*Rank: ");
        tfRank = new TextField(10);
        lblReligion = new JLabel("*Religion: ");
        tfReligion = new TextField(10);
        
        panelInner.add(labelFirst);
        panelInner.add(tfFirst);
        panelInner.add(labelMiddle);
        panelInner.add(tfMiddle);
        panelInner.add(labelLast);
        panelInner.add(tfLast);
        panelInner.add(lblBDay);
        panelInner.add(tfBDay);
        panelInner.add(lblAge);
        panelInner.add(tfAge);  
        group.add(rdBtnM);
        group.add(rdBtnFM);
        panelInner.add(rdBtnM);
        panelInner.add(rdBtnFM);
        panelInner.add(rdBtnFM);
        panelInner.add(lblAddress);
        panelInner.add(taAddress);
        panelInner.add(lblBT);
        panelInner.add(tfBT);
        panelInner.add(lblSN);
        panelInner.add(tfSN);
        panelInner.add(lblUnit);
        panelInner.add(tfUnit);
        panelInner.add(lblUnitAdd);
        panelInner.add(tfUnitAdd);
        panelInner.add(lblRank);
        panelInner.add(tfRank);
        panelInner.add(lblReligion);
        panelInner.add(tfReligion);        
        //panelInner.add(datePicker);
        //panelOuter.add(labelFirst);
        
        panelCenter.add(tabbedPane1);
        panelCenter.setBackground(Color.white);
        panelInner.setBackground(Color.white);
        panelOuter.setBackground(Color.white);
        panel.add(panelInner, BorderLayout.PAGE_START);
        panel.add(panelCenter, BorderLayout.CENTER);
        panel.add(panelOuter, BorderLayout.PAGE_END);
        
        return panel;
    }

    private PanelandTable createHistoryTab(String title, int tab_id) {
        PanelandTable pat = new PanelandTable();
        
        JComponent panel1 = makeTextPanel(title);
        
        JLabel jl = new JLabel("Search Name");
        JTextField jtf = new JTextField(30);
        JButton jb = new JButton("Search");
        JComponent panel2 = makeTextPanel(title);
        panel2.setLayout(new BorderLayout());
        panel2.add(jl, BorderLayout.WEST);
        panel2.add(jtf, BorderLayout.CENTER);
        panel2.add(jb, BorderLayout.LINE_END);

        ExcelData ed = database.getHistory(1, 1);
        String[] columnNames = ed.getColumns();
        Object[][] data = ed.getData();

        JTable table = new JTable(data, columnNames);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setVisible(true);
        table.setFillsViewportHeight(true);
        panel1.setLayout(new BorderLayout());
        panel1.add(table.getTableHeader(), BorderLayout.PAGE_START);
        panel1.add(scrollPane, BorderLayout.CENTER);        
        
        pat.setPanel(panel1);
        pat.setTable(table);
        return pat;
    }
    
    //Load Patient File when ID selected
    private void retrievePatientFile(int patientID, int selectTab) {
        if(patientID != 0) tabbedPane.setSelectedIndex(selectTab);
        
        this.retrieveSetPatientInformation(patientID);
        this.retrieveSetTables(patientID, 3, JThistory);
        this.retrieveSetTables(patientID, 4, JTallergies);
        this.retrieveSetTables(patientID, 5, JTsurgeries);
        this.retrieveSetTables(patientID, 6, JTfamily);        
        this.retrieveSetTables(patientID, 7, JTsocial);
        this.retrieveSetTables(patientID, 8, JTillness);
        this.retrieveSetTables(patientID, 9, JTmedicines);
        this.retrieveSetTables(patientID, 10, JTBP);
    }
    
    //Load Personal Info
    private void retrieveSetPatientInformation(int patientID) {
        try {
            ArrayList<String> patientsArray = database.getPatientsData(patientID);

            tfFirst.setText(patientsArray.get(1)); 
            tfMiddle.setText(patientsArray.get(2)); 
            tfLast.setText(patientsArray.get(3)); 
            tfAge.setText(patientsArray.get(4)); 
            tfBDay.setText(patientsArray.get(5)); 
            tfSN.setText(patientsArray.get(6)); 
            tfBT.setText(patientsArray.get(7)); 
            
            if(patientsArray.get(8).toLowerCase().trim().equals("male")) {
                rdBtnM.setSelected(true);
                rdBtnFM.setSelected(false);
            } else {
                rdBtnM.setSelected(false);
                rdBtnFM.setSelected(true);
            }
            
            taAddress.setText(patientsArray.get(9));
            tfUnit.setText(patientsArray.get(10));
            tfUnitAdd.setText(patientsArray.get(11));
            tfRank.setText(patientsArray.get(12));
            tfReligion.setText(patientsArray.get(13));
        } catch (Exception e) {
            tfFirst.setText(""); 
            tfMiddle.setText(""); 
            tfLast.setText(""); 
            tfAge.setText(""); 
            tfBDay.setText(""); 
            tfSN.setText(""); 
            tfBT.setText(""); 
            rdBtnM.setSelected(true);
            taAddress.setText("");
            tfUnit.setText(""); 
            tfUnitAdd.setText(""); 
            tfRank.setText(""); 
            tfReligion.setText(""); 
        }
    }
    
    //Load all History Tables
    private void retrieveSetTables(int patientID, int selectTab, JTable table) {
        try {
            ExcelData ed = database.getHistory(selectTab,patientID);
            String[] columnNames = ed.getColumns();
            Object[][] data = ed.getData();

            TableModel newModel = new DefaultTableModel(data, columnNames);

            table.setModel(newModel);
            ((AbstractTableModel) newModel).fireTableDataChanged();  
        } catch(Exception e) {
            System.out.println("retrieveSetTables" + e);
        }
    }
    
    protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }

    //check if there is no null fields
    private boolean nullFields() {
        if(tfFirst.getText().trim().length() == 0 ||
            tfFirst.getText().trim().length() == 0 ||
            tfMiddle.getText().trim().length() == 0 ||
            tfLast.getText().trim().length() == 0 ||
            taAddress.getText().trim().length() == 0 ||
            tfBT.getText().trim().length() == 0 ||
            tfSN.getText().trim().length() == 0 ||
            tfAge.getText().trim().length() == 0 ||
            tfBDay.getText().trim().length() == 0) {
            
            JOptionPane.showMessageDialog(null, "REQUIRED FIELDS");
            return true;
        } else {
            return false;
        }
    }
    
    //call db to delete personal info
    void deletePatient(int patientID) {
        database.deletePatientFiles(patientID);
    }
    
    //call database to save personal info
    void savePatient(int patientID) {
        System.out.println("-------------NEW SAVE-----------");
        
        String gender = rdBtnM.isSelected() ? "Male" : "Female";
        int newPatientID = database.savePersonalInfo(
                patientID,tfFirst.getText(),
                tfMiddle.getText(), taAddress.getText(), 
                tfLast.getText(), tfBT.getText(), tfSN.getText(), 
                tfAge.getText(),tfBDay.getText(), gender, tfUnit.getText(),tfUnitAdd.getText(),
                tfRank.getText(), tfReligion.getText());
        
        saveHistory(newPatientID);
    }
    
    //call database to save history
    void saveHistory(int patientID) {
        database.saveMedicalHistory(patientID, JThistory, "Medical_History");
        database.saveMedicalHistory(patientID, JTallergies, "Allergies");
        database.saveMedicalHistory(patientID, JTsurgeries, "Surgeries");
        database.saveMedicalHistory(patientID, JTfamily, "Family_History");
        database.saveMedicalHistory(patientID, JTsocial, "Social_History");
        database.saveMedicalHistory(patientID, JTillness, "Illness");
        database.saveMedicalHistory(patientID, JTmedicines, "Medicines");
        database.saveMedicalHistory(patientID, JTBP, "BP");
        
        System.out.println("-------------END SAVE-----------");
    }
    
    public void actionPerformedTest(ActionEvent e) {
        
    }
}   
