/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientdirectory;

import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author sabbaths
 */
public class PanelandTable {
    JComponent panel;
    JTable table;
    
    void setPanel(JComponent tempPanel) {
        this.panel = tempPanel;
    }
    
    void setTable(JTable tempTable) {
        this.table = tempTable;
    }
    
    public JComponent getPanel() {
        return this.panel;
    } 

    public JTable getTable() {
        table.setGridColor(Color.BLACK);
        table.setBorder(new EtchedBorder(EtchedBorder.RAISED));
        return this.table;
    }     
    
}
